/*
 * main.c
 *
 *  Created on: 2016/10/08
 *      Author: �@��Y
 */

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "uart.h"
#include "ad9851.h"
#include "adc.h"
#include "timer.h"
void loop(void);
void init(void){
	uart_init(br57600);
	uart_bind_stdio();
	ad9851_init();
	adc_init();
	PORTC&=~(1<<0);
	DDRC|=(1<<4);
	PORTC&=~(1<<4);
	timer_init(10,loop);
}
long start_freq;
long stop_freq;
int num_of_meas_point;
int scan_requested;
void loop(void){
	static int measureing=0;
	static int measure_count=0;
	if(scan_requested){
		measureing=1;
		measure_count=0;
		PORTC|=(1<<4);
		scan_requested=0;
	}
	if(measureing){
		long freq=((long long)(stop_freq-start_freq)*measure_count)/num_of_meas_point+start_freq;
		ad9851_setfreq(freq);
		_delay_us(100);
		int adc_val=adc_get(0);
		printf("Freq:%ld,Val:%d\r\n",freq,adc_val);
		measure_count++;
		if(measure_count==num_of_meas_point){
			measure_count=0;
			measureing=0;
			PORTC&=~(1<<4);
		}
	}
}
char line[81];
char*argv[5];
int parse_line(char*str,char*arg[],int max_argc){//str->argv
	int argc=1;
	char*tok;
	tok=strtok(str," \r\n");
	arg[0]=tok;
	while(argc<max_argc&&tok!=NULL){
		tok=strtok(NULL," \r\n");
		if(tok==NULL){
			break;
		}
		printf("tok=%s\r\n",tok);
		arg[argc]=tok;
		argc++;
	}
	printf("argc=%d\r\n",argc);
	return argc;
}
int main(void){
	init();
	printf("AD9851 test\r\n");
	long freq=1000;
	ad9851_setfreq(freq);
	while(1){
		fgets(line,sizeof(line),stdin);
		int argc=parse_line(line,argv,sizeof(argv));
		char*cmdname=argv[0];
		if(strcmp(cmdname,"set_freq")==0){//set_freq <freq>
			if(argc==2){
				long freq=strtol(argv[1],0,0);
				ad9851_setfreq(freq);
				printf("set freq=%ld\r\n",freq);
			}
		}
		if(strcmp(cmdname,"get_amplitude")==0){
			int val=adc_get(0);
			printf("adc=%d\r\n",val);
		}
		if(strcmp(cmdname,"scan")==0){//scan <start> <stop> <measurement_nums>
			start_freq=strtol(argv[1],0,0);
			stop_freq=strtol(argv[2],0,0);
			num_of_meas_point=(int)strtol(argv[3],0,0);
			printf("scan %ld[Hz] to %ld[Hz],%d points\r\n",start_freq,stop_freq,num_of_meas_point);
			scan_requested=1;
		}
	}
	return 0;
}
