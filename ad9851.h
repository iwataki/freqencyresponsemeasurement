/*
 * ad9851.h
 *
 *  Created on: 2016/10/08
 *      Author: �@��Y
 */

#ifndef AD9851_H_
#define AD9851_H_

#define AD9851D7 5
#define AD9851WCLK 6
#define AD9851FQUP 7
#define AD9851RST 0
#define AD9851REFCLK 180000000

void ad9851_init(void);

void ad9851_setfreq(long freq);


#endif /* AD9851_H_ */
