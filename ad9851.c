/*
 * ad9851.c
 *
 *  Created on: 2016/10/08
 *      Author: �@��Y
 */


#include <avr/io.h>
#include "ad9851.h"

void ad9851_reset(void){
	PORTB|=(1<<AD9851RST);
	PORTB&=~(1<<AD9851RST);

	PORTD|=(1<<AD9851WCLK);
	PORTD&=~(1<<AD9851WCLK);

	PORTD|=(1<<AD9851FQUP);
	PORTD&=~(1<<AD9851FQUP);

	unsigned char ctrlword[5]={0,0,0,0,1};//f=0,phase=0,pwron,clk=6*ref
	ad9851_sendword(ctrlword);
}

void ad9851_sendword(unsigned char data[]){//5Byte
	int i;
	for(i=0;i<5;i++){
		int j=0;
		for(j=0;j<8;j++){
			if(data[i]&(1<<j)){//1
				PORTD|=(1<<AD9851D7);
			}else{//0
				PORTD&=~(1<<AD9851D7);
			}
			PORTD|=(1<<AD9851WCLK);
			PORTD&=~(1<<AD9851WCLK);
		}
	}
	PORTD|=(1<<AD9851FQUP);
	PORTD&=~(1<<AD9851FQUP);
}

void ad9851_init(void){
	DDRD|=(1<<AD9851D7)|(1<<AD9851FQUP)|(1<<AD9851WCLK);
	DDRB|=(1<<AD9851RST);
	PORTD&=~((1<<AD9851D7)|(1<<AD9851FQUP)|(1<<AD9851WCLK));
	ad9851_reset();
}

void ad9851_setfreq(long freq){
	unsigned char ctrlword[5]={0,0,0,0,1};
	unsigned long long temp=((long long)freq<<32)/AD9851REFCLK;
	unsigned long df_word=(temp&0xffffffff);
	int i;
	for(i=0;i<4;i++){
		ctrlword[i]=(df_word>>(i*8))&0xff;
	}
	ad9851_sendword(ctrlword);
}
