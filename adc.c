/*
 * adc.c
 *
 *  Created on: 2016/10/08
 *      Author: �@��Y
 */

#include <avr/io.h>
#include "adc.h"

void adc_init(){
	ADCSRA=0x86;//ADC enable clk=8000000/64=125kHz
}
int adc_get(int ch){
	ADMUX=ch;
	ADCSRA|=1<<6;//start conv
	while(ADCSRA&(1<<6));//wait for conv finish
	return ADCW;
}

